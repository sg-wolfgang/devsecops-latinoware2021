---
marp: true
theme: uncover
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
---
<!-- _paginate: false -->
# DevSecOps com **OpenSource**
## Análises automatizadas!
Autor: Samuel Gonçalves

---

> #### *O principal objetivo da educação é criar pessoas capazes de fazer coisas novas, e não simplesmente repetir o que outras gerações fizeram.*

*Jean Piaget*

---

![bg right:30% 80%](images/perfil.png)

#### 🇧🇷 **Samuel Gonçalves Pereira**

* Consultor de Tecnologia de **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência
* Mais de 2 mil alunos ensinados
* Músico, Contista, YouTuber e Podcaster

---

### Entre em contato

[https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

---

<style scoped>
  p {
  font-size: 19pt;
  list-style-type: circle;
}
</style>
![bg left:40% 80%](images/qrcode.png)
### **Acesse o código fonte destes slides!**
[https://gitlab.com/sg-wolfgang/devsecops-latinoware2021](https://gitlab.com/sg-wolfgang/devsecops-latinoware2021)

---

### **SORTEIO NO FIM DA PALESTRA!**
![bg right:50% 80%](images/qr_sorteio4linux.png)
##### Inscreva-se e participe!
Escaneie o QRCode ou [clique aqui!](https://sorteio.4linux.com.br/latinoware--devsecops-com-opensource-anlises-automatizadas)

---

## **DevOps**

Cultura
Colaboração
Agilidade

---

## **DevSecOps**

Representa uma evolução natural e necessária na forma como as organizações de desenvolvimento abordam a segurança.

---

No passado, a segurança era 'acrescentada' ao software no final do ciclo de desenvolvimento _(quase como uma reflexão tardia)_ por uma equipe de segurança separada e testada por uma equipe de garantia de qualidade (QA) também separada.

---

### **Benefícios do DevSecOps**

* Entrega de software rápida e com boa relação custo-benefício
* Segurança aprimorada e proativa
* Correção de vulnerabilidade de segurança acelerada
* Automação compatível com o desenvolvimento moderno
* Um processo repetível e adaptativo
---
#### **Cloud Native Security’s 4Cs**
![w:1000px](images/4cs.png)

---
<style scoped>
  p {
  font-size: 15pt;
}
</style>

![center:50% 50%](images/devsecops_framework.png)
**Fonte:** [promovesolucoes.com/devsecops-seguranca-continua-lgpd](https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/)

---
<!-- _paginate: false -->

![bg center](images/DevSecOps-SAST-Pipeline.png)

---

![bg left:30%](images/DevSecOps-SAST-Pipeline.png)
Falando da aplicação, **SAST** e **DAST** são frequentemente usados ​​em conjunto porque o **SAST** não encontra erros durante o tempo de execução e o **DAST** não sinaliza erros de codificação. Nesse caso, **ambos os modelos são aplicáveis e complementares.**

---

## **SAST**
![grayscale blur:3px bg left:40%](images/sast-bg.jpg)
Acrônimo para **Teste Estático de Segurança de Aplicação**, analisa o código fonte dos sistemas. Os testes  são realizados **antes** que o sistema esteja em produção.

---

#### **Vantagens da implementação**

* Ajuda a encontrar problemas mais cedo, antes da implantação;
* Garante a conformidade com as diretrizes e padrões de codificação sem realmente executar o código;
* Como olha diretamente para o seu código, fornece informações detalhadas que sua equipe de engenharia pode usar facilmente para corrigir os problemas.

---

### **Ferramentas para SAST**

<style>
table {
    height: 50%;
    width: 80%;
    font-size: 25px;
    
}
th {
    
}
</style>

Ferramenta        | Linguagem               | Link para acesso
:---------------: |:-----------------------:|:----------------:
Brakeman          | Ruby                    | [https://brakemanscanner.org/](https://brakemanscanner.org/)
SonarQube         | 27 linguagens           | [https://www.sonarqube.org/](https://www.sonarqube.org/)
Bandit            | Python                  | [https://pypi.org/project/bandit/](https://pypi.org/project/bandit/)
Snyk              | Várias linguagens       | [https://snyk.io/](https://snyk.io/)
CPPCheck          | C++                     | [http://cppcheck.sourceforge.net/](http://cppcheck.sourceforge.net/)
Graudit           | Groovy                  | [https://github.com/wireghoul/graudit](https://github.com/wireghoul/graudit)
Dependency Check  | Dependências de Código  | [https://owasp.org/www-project-dependency-check/](https://owasp.org/www-project-dependency-check/)

---
# **DAST**
![grayscale blur:3px bg right:40%](images/dast-bg.jpg)
**Teste Dinâmico de Segurança de Aplicação** testa as interfaces expostas em busca de vulnerabilidades, após a aplicação estar em execução. 

---

#### **Vantagens da implementação**

* Encontra problemas em tempo de execução que não podem ser identificados pela análise estática;
* Identifica mais rapidamente problemas de autenticação e configuração de servidores;
* Lista as falhas que ficam visíveis apenas quando um usuário de fato efetua um login.
--- 

### **Ferramentas para DAST**

Ferramenta       | Link para acesso
:---------------:|:----------------:
OWASP ZAP        | [https://owasp.org/www-project-zap/](https://owasp.org/www-project-zap/)
Arachni          | [https://www.arachni-scanner.com/](https://www.arachni-scanner.com/)
SQLmap           | [http://sqlmap.org/](http://sqlmap.org/)
Gauntlt          | [http://gauntlt.org/](http://gauntlt.org)
BDD Security     | [https://github.com/iriusrisk/bdd-security](https://github.com/iriusrisk/bdd-security)
Nikto            | [https://github.com/sullo/nikto](https://github.com/sullo/nikto)
Golismero        | [https://github.com/golismero/golismero](https://github.com/golismero/golismero)

---
### **Exemplo de Utilização SAST & DAST**

![w:800](images/jenkins_logo.png)

###### *Jenkinsfile utilizado: [https://gitlab.com/sg-wolfgang/sast-dast-juiceshop](https://gitlab.com/sg-wolfgang/sast-dast-juiceshop)* 

---

### **SAST com Sonarqube**

```yaml
stage('SAST com Sonarqube'){
            environment {
                scanner = tool 'sonar-scanner'
            }
            steps{
                withSonarQubeEnv('sonarqube') {
                    sh "${scanner}/bin/sonar-scanner -Dsonar.projectKey=$NAME_APP -Dsonar.sources=${WORKSPACE}/ -Dsonar.projectVersion=${BUILD_NUMBER}"
                }
            }
```
---

### **SonarQube Report**

![w:800](images/soanrqube-scanner-results.png)

---
### **DAST com Golismero**

```yaml
stage('DAST com Golismero'){
    environment {
        APP_URL = "http://juiceshop.imsafe.com.br/"
    }
    agent { node 'sentinel' }
    steps{
        sh "golismero scan $APP_URL -o report${BUILD_NUMBER}.html"
    
        publishHTML([
            allowMissing: false,
            alwaysLinkToLastBuild: true,
            keepAll: true,
            reportDir: '/report-location/',
            reportFiles: 'dast-report-${BUILD_NUMBER}.html',
            reportName: 'DAST REPORT'
        ])
    }
```
---

### **Golismero Report**

![w:700](images/golismero-report.png)

---
#### Dúvidas?

![bg blur:1px left:70%](images/duvida.jpg)

---

## **Obrigado!**

Vamos nos conectar?
* **Site:** [sgoncalves.tec.br](https://sgoncalves.tec.br)
* **E-mail:** [samuel@sgoncalves.tec.br](https://sgoncalves.tec.br/contato)
* **Linkedin:** [linkedin.com/in/samuelgoncalvespereira/](linkedin.com/in/samuelgoncalvespereira/)
* **Telegram:** [t.me/Samuel_gp](t.me/Samuel_gp)
* **Todas as redes:** [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

---
### Fontes Bibliográficas

<style scoped>
  p {
  font-size: 19pt;
  list-style-type: circle;
}
</style>
[https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/](https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/)
[https://www.redhat.com/pt-br/topics/devops/what-is-devsecops](https://www.redhat.com/pt-br/topics/devops/what-is-devsecops)
[https://www.ibm.com/br-pt/cloud/learn/devsecops](https://www.ibm.com/br-pt/cloud/learn/devsecops)
[https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/](https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/)
[https://4linux.com.br/cursos/treinamento/devsecops-seguranca-em-infraestrutura-e-desenvolvimento-agil/](https://4linux.com.br/cursos/treinamento/devsecops-seguranca-em-infraestrutura-e-desenvolvimento-agil/)
[https://blog.4linux.com.br/devsecops-implementacao-em-6-passos/](https://blog.4linux.com.br/devsecops-implementacao-em-6-passos/)