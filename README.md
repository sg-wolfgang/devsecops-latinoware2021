# Slides da palestra "DevSecOps com OpenSource: Análises Automatizadas"

Este repositório armazena os slides utilizados na palestra sobre DevSecOps realizada na Latinoware 2021 por Samuel Gonçalves.

Os slides foram escritos utilizando Markdown e estilizados com **marp**.

Seguem links de referência:
* [https://github.com/marp-team/marp](https://github.com/marp-team/marp)
* [https://www.markdownguide.org/](https://www.markdownguide.org/)

Dentro do diretório "slides" existe um arquivo chamado "slides.pdf" já estilizado. 

[Você pode clicar aqui para realizar o download do arquivo diretamente, caso prefira.](https://gitlab.com/sg-wolfgang/devsecops-latinoware2021/-/raw/main/slides/slides.pdf?inline=false)